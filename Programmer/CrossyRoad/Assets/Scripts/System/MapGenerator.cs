using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public enum LaneType
{
    GRASS,
    ROAD,
    RIVER,
    RAILWAY
}


struct ClearPath
{
    public Vector3Int coords;
    public bool justTurned;
    public int initialX;
    public int range;
}


public class MapGenerator : MonoBehaviour
{
    #region Variables & Properties
    GameManager gameManagerRef;
    Lane lastLane = null;
    Lane savedLane = null;
    Queue<Lane> lanesQueue = new Queue<Lane>();
    List<ClearPath> clearPaths;
    Collider mapTrigger;
    RaycastHit coinSpawnHit;
    GameObject spawnedCoin;

    const int walkableLanes = 9;
    bool[] clearPathRoutes = new bool[walkableLanes];
    int coinLanesCounter;
    int bestDistance;
    int mapTheme;

    [Space(20)]
    [Header("Generation Parameters")]
    [Tooltip("How long should the map be at all times in the z axis")]
    [SerializeField] int lanesSpawnRange;
    [Tooltip("How long should the spawn point be in the z axis starting from lane zero")]
    [SerializeField] int spawnLength;
    [Range(0, 3)]
    [Tooltip("How many pathways should always be kept clear")]
    [SerializeField] int clearPathsCount;
    [Range(0, 3)]
    [Tooltip("How far on the x axis can a path move from its starting point")]
    [SerializeField] int clearPathsMaxSpreadRange;
    [Tooltip("Probability of an internal obstacle of being spawned if it's in a suitable location")]
    [SerializeField] int internalObstaclesSpawnProbability;
    [Tooltip("Probability of an external obstacle of being spawned if it's in a suitable location")]
    [SerializeField] int externalObstaclesSpawnProbability;
    [Tooltip("How many minimum lanes there will be between each coin (WITHOUT COUNTING RIVER LANES)")]
    [SerializeField] int minLanesBeforeCoinSpawn;
    [Tooltip("How many maximum lanes there will be between each coin (WITHOUT COUNTING RIVER LANES)")]
    [SerializeField] int maxLanesBeforeCoinSpawn;

    [Space(20)]
    [Header("Debug Parameters")]
    [Tooltip("Shows debug information regarding the map generator")]
    [SerializeField] bool debugMode;

    [Space(20)]
    [Header("Spawnable Items References")]
    [Tooltip("Reference to the spawnable coin")]
    [SerializeField] GameObject coin;
    [Tooltip("Reference to the spawnable water coin")]
    [SerializeField] GameObject shell;
    [Tooltip("The record distance text prefab reference")]
    [SerializeField] GameObject text_BestDistance;

    [Space(20)]
    [Header("Lanes References")]
    [Tooltip("All the references to all spawnable lanes")]
    [SerializeField] Lane[] lanes;
    [Tooltip("All the references to all spawnable water lanes")]
    [SerializeField] Lane[] waterLanes;

    public GameManager GameManager { set { gameManagerRef = value; } }
    public int BestDistance { set { bestDistance = value; } }
    public int MapTheme { get { return mapTheme; } set { mapTheme = value; } }
    #endregion


    #region Mono
    //Generate spawn area on start and sets out of bounds collider
    private void Start()
    {
        mapTrigger = GetComponent<Collider>();
        mapTrigger.transform.localScale = new Vector3(walkableLanes, mapTrigger.transform.localScale.y, lanesSpawnRange/5);
        GenerateSpawn();
    }


    //If player gets out of bounds collider, game over
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Player>() != null)
            other.gameObject.GetComponent<Player>().GameOver(GameOverType.OUT_OF_BOUNDS);
    }
    #endregion


    #region Methods
    /// <summary>
    /// Loads map theme from GameInstance
    /// </summary>
    public void LoadMapTheme()
    {
        if (GameInstance.CharsThemes[GameInstance.SelectedChar] != mapTheme)
            mapTheme = GameInstance.CharsThemes[GameInstance.SelectedChar];
    }


    /// <summary>
    /// Generates the initial spawn location
    /// </summary>
    public void GenerateSpawn()
    {
        LoadMapTheme();

        mapTrigger.transform.position = Vector3.zero;

        for (int z = -lanesSpawnRange; z <= lanesSpawnRange; z++)
        {
            if (z < spawnLength || lastLane == null)
                GenerateNextLane(true, z);
            else
                GenerateNextLane(false, z);
        }
    }


    /// <summary>
    /// Updates lanes queue, popping the last one and pushing a new one
    /// </summary>
    /// <param name="newLanePosition">The new layer z position</param>
    public void UpdateLanesQueue(int newLanePosition)
    {
        mapTrigger.transform.position += Vector3.forward;
        lastLane = lanesQueue.Dequeue();
        DestroyLane(lastLane);
        
        GenerateNextLane(false, newLanePosition + lanesSpawnRange);
    }


    /// <summary>
    /// Generates next lane in z position
    /// </summary>
    /// <param name="spawn">Is it a spawn lane</param>
    /// <param name="z">Lane z coordinate location</param>
    void GenerateNextLane(bool spawn, int z)
    {
        if (spawn)
        {
            lastLane = SelectThemedLane(z, true, 0);
            lanesQueue.Enqueue(lastLane);

            if (z < -spawnLength)
                PlaceObstacles(lastLane, ObstaclesDistribution.BACKSPAWN, null);
            else
                PlaceObstacles(lastLane, ObstaclesDistribution.SPAWN, savedLane);

            savedLane = lastLane;
        }
        else
        {
            /*TEMPORARY CODE*/
            if (savedLane != null && savedLane.type == LaneType.RIVER)
            {
                int noRiver = Random.Range(0, 3);
                if (noRiver == 2)
                    noRiver++;

                lastLane = SelectThemedLane(z, true, noRiver);
            }
            /*END OF TEMPORARY CODE*/
            else
                lastLane = SelectThemedLane(z, false, 0);

            lanesQueue.Enqueue(lastLane);

            CalculateClearPathways(z);
            PlaceObstacles(lastLane, ObstaclesDistribution.NORMAL, savedLane);

            savedLane = lastLane;
        }
    }


    /// <summary>
    /// Returns lane based on selected map theme
    /// </summary>
    /// <param name="z">The lane required z spawn location</param>
    /// <param name="forcePick">Should lane type be forcefully picked</param>
    /// <param name="pick">The forcefully picked lane type</param>
    /// <returns></returns>
    Lane SelectThemedLane(int z, bool forcePick, int pick)
    {
        switch (mapTheme)
        {
            case 0:
                if (forcePick)
                    return Instantiate(lanes[pick], new Vector3(0f, 0f, z), Quaternion.identity).GetComponent<Lane>();
                return Instantiate(lanes[Random.Range(0, lanes.Length)], new Vector3(0f, 0f, z), Quaternion.identity).GetComponent<Lane>();

            case 1:
                if (forcePick)
                    return Instantiate(waterLanes[pick], new Vector3(0f, 0f, z), Quaternion.identity).GetComponent<Lane>();
                return Instantiate(waterLanes[Random.Range(0, waterLanes.Length)], new Vector3(0f, 0f, z), Quaternion.identity).GetComponent<Lane>();

            default: 
                return null;
        }
    }


    /// <summary>
    /// Calculates clear path for new generated lane
    /// </summary>
    /// <param name="z">New lane position</param>
    void CalculateClearPathways(int z)
    {
        //If no pathway exists, initialize them
        if (clearPaths == null)
        {
            clearPaths = new List<ClearPath>();
            int xCoord;

            for (int i = 0; i < clearPathsCount; i++)
            {
                xCoord = (i == 0 ? 0 : i == 1 ? 2 : -2);

                ClearPath clearPath = new ClearPath { coords = new Vector3Int(xCoord, 0, z), justTurned = false, initialX = xCoord, range = clearPathsMaxSpreadRange };
                clearPaths.Add(clearPath);

                clearPathRoutes[clearPaths[i].coords.x + walkableLanes/2] = true;
            }

            if (debugMode)
            {
                for (int i = 0; i < clearPathRoutes.Length; i++)
                {
                    if (!clearPathRoutes[i])
                        Debug.DrawRay(new Vector3(i - walkableLanes / 2, clearPaths[0].coords.y, clearPaths[0].coords.z), Vector3.up * .3f, Color.black, 3600f);
                }
            }
        }
        else
        {
            //Sort paths based on distance
            clearPaths = SortPathPoints(clearPaths);
            //Clear previous clearPathRoutes array
            for (int i = 0; i < clearPathRoutes.Length; i++)
                clearPathRoutes[i] = false;

            int probability = 0;
            bool mustTurn = false;

            //For each path point, advance and try to turn
            for (int i = 0; i < clearPaths.Count; i++)
            {
                if (Mathf.Abs(clearPaths[i].coords.x) == walkableLanes / 2)
                {
                    if (clearPaths[i].coords.x > 0)
                    {
                        if (!clearPathRoutes[(clearPaths[i].coords.x - 1) + walkableLanes / 2])
                            TurnPath(-1, i);
                        else 
                            TurnPath(0, i);
                    }
                    else
                    {
                        if (!clearPathRoutes[(clearPaths[i].coords.x + 1) + walkableLanes / 2])
                            TurnPath(1, i);
                        else
                            TurnPath(0, i);
                    }
                }
                else
                {
                    if (clearPathRoutes[(clearPaths[i].coords.x) + walkableLanes / 2])
                        mustTurn = true;
                    else
                    {
                        if (mustTurn)
                            mustTurn = false;

                        probability = Random.Range(1, 101);
                    }

                    if ((probability < 30 || mustTurn) && (clearPaths[i].coords.x - 1 >= clearPaths[i].initialX - clearPaths[i].range) && !clearPathRoutes[(clearPaths[i].coords.x - 1) + walkableLanes / 2])
                        TurnPath(-1, i);
                    else if ((probability < 60 || mustTurn) && (clearPaths[i].coords.x + 1 <= clearPaths[i].initialX + clearPaths[i].range) && !clearPathRoutes[(clearPaths[i].coords.x + 1) + walkableLanes / 2])
                        TurnPath(1, i);
                    else
                        TurnPath(0, i);
                }
            }

            //If debugMode is active, draw suitable obstacles locations
            if (debugMode)
            {
                for (int i = 0; i < clearPathRoutes.Length; i++)
                {
                    if (!clearPathRoutes[i])
                        Debug.DrawRay(new Vector3(i - walkableLanes / 2, clearPaths[0].coords.y, clearPaths[0].coords.z), Vector3.up * 0.3f, Color.black, 3600f);
                }
            }
        }
    }


    /// <summary>
    /// Turn clear path in selected direction (if possible)
    /// </summary>
    /// <param name="direction">New direction to turn towards</param>
    /// <param name="i">Clear path index</param>
    void TurnPath(int direction, int i)
    {
        clearPathRoutes[clearPaths[i].coords.x + walkableLanes / 2] = true;

        if (direction == 1 && !clearPaths[i].justTurned)
        {
            if (debugMode)
            {
                Debug.DrawRay(clearPaths[i].coords, Vector3.up, Color.red, 3600f);
                Debug.DrawRay(clearPaths[i].coords + Vector3.forward, Vector3.up, Color.red, 3600f);
                Debug.DrawRay(clearPaths[i].coords + Vector3.forward + Vector3.right, Vector3.up, Color.red, 3600f);

                Debug.DrawRay(clearPaths[i].coords, Vector3.forward, Color.blue, 3600f);
                Debug.DrawRay(clearPaths[i].coords + Vector3.forward, Vector3.right, Color.blue, 3600f);
            }

            ClearPath tmpClearPath = new ClearPath {coords = new Vector3Int(clearPaths[i].coords.x + 1, clearPaths[i].coords.y, clearPaths[i].coords.z + 1), justTurned = true, initialX = clearPaths[i].initialX, range = clearPaths[i].range };
            clearPaths[i] = tmpClearPath;

            clearPathRoutes[clearPaths[i].coords.x + walkableLanes / 2] = true;
        }
        else if (direction == -1 && !clearPaths[i].justTurned)
        {
            if (debugMode)
            {
                Debug.DrawRay(clearPaths[i].coords, Vector3.up, Color.red, 3600f);
                Debug.DrawRay(clearPaths[i].coords + Vector3.forward, Vector3.up, Color.red, 3600f);
                Debug.DrawRay(clearPaths[i].coords + Vector3.forward + Vector3.left, Vector3.up, Color.red, 3600f);

                Debug.DrawRay(clearPaths[i].coords, Vector3.forward, Color.blue, 3600f);
                Debug.DrawRay(clearPaths[i].coords + Vector3.forward, Vector3.left, Color.blue, 3600f);
            }

            ClearPath tmpClearPath = new ClearPath { coords = new Vector3Int(clearPaths[i].coords.x - 1, clearPaths[i].coords.y, clearPaths[i].coords.z + 1), justTurned = true, initialX = clearPaths[i].initialX, range = clearPaths[i].range };
            clearPaths[i] = tmpClearPath;

            clearPathRoutes[clearPaths[i].coords.x + walkableLanes / 2] = true;
        }
        else if (direction == 0 || clearPaths[i].justTurned)
        {
            if (debugMode)
            {
                Debug.DrawRay(clearPaths[i].coords, Vector3.up, Color.red, 3600f);
                Debug.DrawRay(clearPaths[i].coords + Vector3.forward, Vector3.up, Color.red, 3600f);

                Debug.DrawRay(clearPaths[i].coords, Vector3.forward, Color.blue, 3600f);
            }

            ClearPath tmpClearPath = new ClearPath { coords = new Vector3Int(clearPaths[i].coords.x, clearPaths[i].coords.y, clearPaths[i].coords.z + 1), justTurned = false, initialX = clearPaths[i].initialX, range = clearPaths[i].range };
            clearPaths[i] = tmpClearPath;
        }
    }


    /// <summary>
    /// Sorts path points based on shortest distance first
    /// </summary>
    /// <param name="clearPaths">Paths to be sorted</param>
    /// <returns></returns>
    List<ClearPath> SortPathPoints(List<ClearPath> clearPaths)
    {
        List<ClearPath> sortedPaths = new List<ClearPath>();
        int minDistance = walkableLanes;
        int minVectorIndex = 0;
        int vectorCount = clearPaths.Count;

        for (int i = 0; i < vectorCount; i++)
        {
            for (int j = 0; j < vectorCount - i; j++)
            {
                if (Mathf.Abs(clearPaths[j].coords.x) <= minDistance)
                {
                    minVectorIndex = j;
                    minDistance = (int)Mathf.Abs(clearPaths[j].coords.x);
                }
            }
            sortedPaths.Add(clearPaths[minVectorIndex]);
            clearPaths.RemoveAt(minVectorIndex);
            minDistance = walkableLanes;
        }

        return sortedPaths;
    }


    /// <summary>
    /// Place obstacles in selected lane
    /// </summary>
    /// <param name="lane">Lane to populate</param>
    /// <param name="distribution">How should obstacles be placed</param>
    void PlaceObstacles(Lane lane, ObstaclesDistribution distribution, Lane lastLane)
    {
        if (lane.transform.position.z == bestDistance)
            SpawnBestDistanceText(lane);

        switch (lane.type)
        {
            case LaneType.GRASS:
                lane.GetComponent<GrassLane>().InitializeLane(clearPathRoutes, internalObstaclesSpawnProbability, externalObstaclesSpawnProbability, distribution);
                break;

            case LaneType.ROAD:
                if (lastLane.type == LaneType.ROAD)
                    lane.GetComponent<RoadLane>().InitializeLane(true);
                else
                    lane.GetComponent<RoadLane>().InitializeLane(false);
                break;

            case LaneType.RIVER:
                lane.GetComponent<RiverLane>().InitializeLane(clearPathRoutes, internalObstaclesSpawnProbability, externalObstaclesSpawnProbability, distribution);
                break;

            case LaneType.RAILWAY:
                lane.GetComponent<RailwayLane>().InitializeLane();
                break;
        }

        //Coin spawn
        TrySpawningCoin(lane, distribution);
    }


    /// <summary>
    /// Tries spawning a coin (and adds it to the lane items) if lane type and distribution is suitable
    /// </summary>
    /// <param name="lane">The lane type</param>
    /// <param name="distribution">The lane distribution pattern</param>
    void TrySpawningCoin(Lane lane, ObstaclesDistribution distribution)
    {
        if (distribution == ObstaclesDistribution.NORMAL && lane.type != LaneType.RIVER)
        {
            if (coinLanesCounter == 0)
                coinLanesCounter = Random.Range(minLanesBeforeCoinSpawn, maxLanesBeforeCoinSpawn + 1);
            coinLanesCounter--;

            if (coinLanesCounter <= 0)
            {
                coinLanesCounter = Random.Range(0, clearPathsCount);
                for (int i = 0; i < clearPathRoutes.Length; i++)
                {
                    if (clearPathRoutes[i])
                    {
                        if (coinLanesCounter <= 0)
                        {
                            Physics.Raycast(new Vector3(i - walkableLanes / 2, .5f, lane.transform.position.z), Vector3.down, out coinSpawnHit);
                            switch (mapTheme)
                            {
                                case 0:
                                    spawnedCoin = Instantiate(coin, coinSpawnHit.point, Quaternion.identity);
                                    break;

                                case 1:
                                    spawnedCoin = Instantiate(shell, coinSpawnHit.point, Quaternion.identity);
                                    break;
                            }
                            spawnedCoin.GetComponent<Coin>().GameManager = gameManagerRef;

                            switch (lane.type)
                            {
                                case LaneType.GRASS:
                                    lane.GetComponent<GrassLane>().AddItem(spawnedCoin);
                                    break;

                                case LaneType.ROAD:
                                    lane.GetComponent<RoadLane>().AddItem(spawnedCoin);
                                    break;

                                case LaneType.RAILWAY:
                                    lane.GetComponent<RailwayLane>().AddItem(spawnedCoin);
                                    break;
                            }

                            if (debugMode)
                                Debug.DrawRay(spawnedCoin.transform.position, Vector3.up * 5f, Color.yellow, 3600f);

                            break;
                        }
                        else
                            coinLanesCounter--;
                    }
                }
                coinLanesCounter = 0;
            }
        }
    }


    /// <summary>
    /// Spawns the record distance text
    /// </summary>
    /// <param name="lane">The lane where the text should be spawned</param>
    void SpawnBestDistanceText(Lane lane)
    {
        GameObject bestDistanceText;
        RaycastHit hit;
        if (Physics.Raycast(lane.transform.position + Vector3.up * .45f, Vector3.down, out hit))
        {
            bestDistanceText = Instantiate(text_BestDistance, hit.point + Vector3.up * .001f, Quaternion.Euler(90, 0, 0));
            bestDistanceText.GetComponent<TMP_Text>().text = "--- TOP " + bestDistance + " --- TOP " + bestDistance + " --- TOP " + bestDistance + " --- TOP " + bestDistance + " --- TOP " + bestDistance + " --- TOP " + bestDistance + " --- TOP " + bestDistance + " ---";

            switch (lane.type)
            {
                case LaneType.GRASS:
                    lane.GetComponent<GrassLane>().AddItem(bestDistanceText);
                    break;

                case LaneType.ROAD:
                    lane.GetComponent<RoadLane>().AddItem(bestDistanceText);
                    break;

                case LaneType.RIVER:
                    bestDistanceText.transform.position += Vector3.up * .3f;
                    lane.GetComponent<RiverLane>().AddItem(bestDistanceText);
                    break;

                case LaneType.RAILWAY:
                    lane.GetComponent<RailwayLane>().AddItem(bestDistanceText);
                    break;
            }
        }
    }


    /// <summary>
    /// Destroys all map lanes and items
    /// </summary>
    public void DestroyMap()
    {
        for (int i = 0; i < 2 * lanesSpawnRange + 1; i++)
        {
            lastLane = lanesQueue.Dequeue();
            DestroyLane(lastLane);
        }
    }


    /// <summary>
    /// Destroys a single map lane (and all its items)
    /// </summary>
    /// <param name="lane">The lane to be destroyed</param>
    void DestroyLane(Lane lane)
    {
        switch (lastLane.type)
        {
            case LaneType.GRASS:
                lastLane.GetComponent<GrassLane>().DestroyLane();
                break;

            case LaneType.ROAD:
                lastLane.GetComponent<RoadLane>().DestroyLane();
                break;

            case LaneType.RIVER:
                lastLane.GetComponent<RiverLane>().DestroyLane();
                break;

            case LaneType.RAILWAY:
                lastLane.GetComponent<RailwayLane>().DestroyLane();
                break;
        }
    }
    #endregion
}
