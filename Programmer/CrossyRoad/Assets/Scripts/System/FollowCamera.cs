using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    #region Variables & Properties
    Vector3 cameraStartingPosition;
    Vector3 cameraOffset;
    Vector3 velocity = Vector3.zero;
    Vector3 followPosition;
    Vector3 panningPoint;
    Transform targetToFollow;

    bool isInitialized;
    bool isGameRunning;

    [Space(20)]
    [Header("Camera Settings")]
    [Tooltip("How often (seconds) should camera update panning position")]
    [SerializeField] float cameraUpdateTime;
    [Tooltip("Approximately the time it will take to reach the target")]
    [SerializeField] float smoothingTime;
    [Tooltip("How many lanes per second will the camera be panning forward")]
    [SerializeField] float panningSpeed;
    #endregion


    #region Mono
    //Gets camera starting position for repositioning
    private void Start()
    {
        cameraStartingPosition = transform.position;
    }


    //Camera always follows panning point
    private void Update()
    {
        if (!isInitialized) return;

        followPosition = targetToFollow.position + cameraOffset;
        transform.position = Vector3.SmoothDamp(transform.position, panningPoint, ref velocity, smoothingTime);
    }


    //If player gets out of camera view, game over
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Player>() != null)
            other.gameObject.GetComponent<Player>().GameOver(GameOverType.OUT_OF_CAMERA);
    }
    #endregion


    #region Methods
    /// <summary>
    /// Sets up follow camera, assigning target positions, starting the update cycle
    /// </summary>
    /// <param name="target">Target to follow</param>
    public void InitializeCamera(Transform target)
    {
        isGameRunning = true;
        targetToFollow = target;

        cameraOffset = transform.position - targetToFollow.position;
        panningPoint = cameraOffset;
        StartCoroutine(CameraPanning());

        isInitialized = true;
    }


    /// <summary>
    /// When game is over, stops camera panning
    /// </summary>
    public void GameOver()
    {
        isGameRunning = false;
        isInitialized = false;
    }


    /// <summary>
    /// Pans camera backwards to show the eagle animation during a out of camera game over
    /// </summary>
    /// <returns></returns>
    public IEnumerator EagleGameOver()
    {
        isGameRunning = false;
        panningPoint -= Vector3.forward * 3f;

        yield return new WaitForSeconds(1f);

        isInitialized = false;
    }


    /// <summary>
    /// Reloads camera on new game
    /// </summary>
    public void ReloadCamera()
    {
        transform.position = cameraStartingPosition;
        velocity = Vector3.zero;
    }


    /// <summary>
    /// Constantly updates camera panning position while game is running
    /// </summary>
    /// <returns></returns>
    IEnumerator CameraPanning()
    {
        while (isGameRunning)
        {
            if (targetToFollow.position.z > panningPoint.z - cameraOffset.z)
                panningPoint = new Vector3(targetToFollow.position.x + cameraOffset.x, targetToFollow.position.y + cameraOffset.y, targetToFollow.position.z + cameraOffset.z);
            
            panningPoint = new Vector3(targetToFollow.position.x + cameraOffset.x, targetToFollow.position.y + cameraOffset.y, panningPoint.z + panningSpeed*cameraUpdateTime);
            yield return new WaitForSeconds(cameraUpdateTime);
        }
    }
    #endregion
}
