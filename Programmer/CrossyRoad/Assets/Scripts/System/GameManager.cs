using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    #region Variables & Properties
    Player playerRef;
    GameObject eagle;

    int travelDistance;
    int maxTravelDistance;
    int bestDistance;
    int coins;

    [Space(20)]
    [Header("Prefabs References")]
    [Tooltip("The player prefab reference")]
    [SerializeField] GameObject prefab_Player;
    [Tooltip("The eagle prefab reference")]
    [SerializeField] GameObject prefab_Eagle;
    [Tooltip("The mantaray prefab reference")]
    [SerializeField] GameObject prefab_Mantaray;

    [Space(20)]
    [Header("GameObjects References")]
    [Tooltip("The sound manager reference")]
    [SerializeField] SoundManager soundManagerRef;
    [Tooltip("The follow camera reference")]
    [SerializeField] FollowCamera followCameraRef;
    [Tooltip("The map generator reference")]
    [SerializeField] MapGenerator mapGeneratorRef;
    [Tooltip("The gameUI reference")]
    [SerializeField] GameUI gameUIRef;

    [Space(20)]
    [Header("Audio Clips References")]
    [Tooltip("The normal jump audio clip reference")]
    [SerializeField] AudioClip ac_Jump;
    [Tooltip("The water jump audio clip reference")]
    [SerializeField] AudioClip ac_WaterJump;

    public int Coins { get { return coins; } set { coins = value; } }
    public Player PlayerRef { get { return playerRef; } }
    public MapGenerator MapGenerator { get { return mapGeneratorRef;} }
    #endregion


    #region Mono
    //Initializes game on start
    private void Start()
    {
        InitializeNewGame();
    }
    #endregion


    #region Methods
    /// <summary>
    /// Starts a new game, initializing the player and the camera
    /// </summary>
    void InitializeNewGame()
    {
        GameInstance.LoadSaves();
        bestDistance = GameInstance.BestDistance;
        coins = GameInstance.Coins;

        mapGeneratorRef.GameManager = this;
        mapGeneratorRef.LoadMapTheme();
        mapGeneratorRef.BestDistance = bestDistance;

        gameUIRef.LoadFilters(mapGeneratorRef.MapTheme);

        playerRef = Instantiate(prefab_Player, new Vector3(0f, .5f, 0f), Quaternion.identity).GetComponent<Player>();
        playerRef.GameManager = this;
        
        //Selects player jumping sound based on map theme
        switch (mapGeneratorRef.MapTheme)
        {
            case 0:
                playerRef.SetJumpSound(ac_Jump);
                break;

            case 1:
                playerRef.SetJumpSound(ac_WaterJump);
                break;
        }
    }


    /// <summary>
    /// Initializes camera and enables player movement on start
    /// </summary>
    public void StartGame()
    {
        gameUIRef.UpdateDistanceScore(0);
        gameUIRef.UpdateCoins(coins);

        followCameraRef.InitializeCamera(playerRef.transform);
        playerRef.CanMove = true;
    }


    /// <summary>
    /// Ends current game depending on game over <type> and saves progress
    /// </summary>
    /// <param name="type">The game over type</param>
    /// <returns></returns>
    public IEnumerator GameOver(GameOverType type)
    {
        switch (type)
        {
            case GameOverType.OUT_OF_CAMERA:
                StartCoroutine(followCameraRef.EagleGameOver());
                switch (mapGeneratorRef.MapTheme)
                {
                    case 0:
                        eagle = Instantiate(prefab_Eagle, playerRef.transform.position + Vector3.forward * 10f + Vector3.up * 5f, Quaternion.identity);
                        break;

                    case 1:
                        eagle = Instantiate(prefab_Mantaray, playerRef.transform.position + Vector3.forward * 10f + Vector3.up * 5f, Quaternion.identity);
                        break;
                }
                eagle.GetComponent<Eagle>().StartChasing(playerRef.gameObject);
                break;

            case GameOverType.OUT_OF_BOUNDS:
                followCameraRef.GameOver();
                break;

            case GameOverType.RUN_OVER:
                soundManagerRef.PlaySound(SoundType.RUN_OVER);
                yield return new WaitForSeconds(1f);
                followCameraRef.GameOver();
                break;

            case GameOverType.RUN_OVER_FAST:
                soundManagerRef.PlaySound(SoundType.RUN_OVER);
                yield return new WaitForSeconds(.5f);
                followCameraRef.GameOver();
                break;

            case GameOverType.DROWNED:
                switch (mapGeneratorRef.MapTheme)
                {
                    case 0:
                        soundManagerRef.PlaySound(SoundType.DROWNED);
                        break;

                    case 1:
                        soundManagerRef.PlaySound(SoundType.DROWNED_ALT);
                        break;
                }
                Destroy(playerRef.gameObject);
                followCameraRef.GameOver();
                break;
        }
        
        gameUIRef.SwitchToGameOverUI(maxTravelDistance, bestDistance);

        if (maxTravelDistance > bestDistance)
        {
            bestDistance = travelDistance;
            GameInstance.BestDistance = bestDistance;
        }

        GameInstance.Coins = coins;
        GameInstance.WriteSaves();
    }


    /// <summary>
    /// Reloads map initializing a new game
    /// </summary>
    public void ReloadMap()
    {
        if (eagle != null)
            Destroy(eagle);
        if (playerRef != null)
            Destroy(playerRef.gameObject);

        mapGeneratorRef.DestroyMap();

        mapGeneratorRef.GenerateSpawn();
        mapGeneratorRef.BestDistance = bestDistance;

        gameUIRef.LoadFilters(mapGeneratorRef.MapTheme);

        playerRef = Instantiate(prefab_Player, new Vector3(0f, .5f, 0f), Quaternion.identity).GetComponent<Player>();
        playerRef.GameManager = this;

        switch (mapGeneratorRef.MapTheme)
        {
            case 0:
                playerRef.SetJumpSound(ac_Jump);
                break;

            case 1:
                playerRef.SetJumpSound(ac_WaterJump);
                break;
        }

        travelDistance = 0;
        maxTravelDistance = 0;
        gameUIRef.UpdateDistanceScore(0);

        followCameraRef.ReloadCamera();
    }


    /// <summary>
    /// Checks if the player lane is a new one and updates all information
    /// </summary>
    /// <param name="newPlayerPosition">The new player position</param>
    public void CheckNewPlayerPosition(Vector3 newPlayerPosition)
    {

        travelDistance = Mathf.RoundToInt(newPlayerPosition.z);
        if (travelDistance > maxTravelDistance)
        {
            maxTravelDistance = travelDistance;

            gameUIRef.UpdateDistanceScore(maxTravelDistance);
            mapGeneratorRef.UpdateLanesQueue(travelDistance);
        }
    }


    /// <summary>
    /// Adds a coin to the counter and UI
    /// </summary>
    public void AddCoin()
    {
        soundManagerRef.PlaySound(SoundType.COIN_COLLECTED);
        gameUIRef.UpdateCoins(++coins);
    }
    #endregion
}
