using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum SoundType
{
    COIN_COLLECTED,
    DROWNED,
    RUN_OVER,
    DROWNED_ALT
}


public class SoundManager : MonoBehaviour
{
    #region Variables & Properties
    AudioSource[] as_sources;

    [Space(20)]
    [Header("AudioClips References")]
    [Tooltip("The coin collected audio clip")]
    [SerializeField] AudioClip ac_CoinCollected;
    [Tooltip("The default player drowned audio clip")]
    [SerializeField] AudioClip ac_Drowned;
    [Tooltip("The player run over audio clip")]
    [SerializeField] AudioClip ac_RunOver;
    [Tooltip("The alternate player drowned audio clip")]
    [SerializeField] AudioClip ac_DrownedAlt;
    #endregion


    #region Mono
    //Gets all the audio sources
    private void Start()
    {
        as_sources = GetComponents<AudioSource>();
    }
    #endregion


    #region Methods
    /// <summary>
    /// Plays sound based on the requested <type>
    /// </summary>
    /// <param name="type"></param>
    public void PlaySound(SoundType type)
    {
        switch (type)
        {
            case SoundType.COIN_COLLECTED:
                if (as_sources[0].clip != ac_CoinCollected)
                    as_sources[0].clip = ac_CoinCollected;
                as_sources[0].Play();
                break;


            case SoundType.DROWNED:
                if (as_sources[0].clip != ac_Drowned)
                    as_sources[0].clip = ac_Drowned;
                as_sources[0].Play();
                break;


            case SoundType.RUN_OVER:
                if (as_sources[0].clip != ac_RunOver)
                    as_sources[0].clip = ac_RunOver;
                as_sources[0].Play();
                break;

            case SoundType.DROWNED_ALT:
                if (as_sources[0].clip != ac_DrownedAlt)
                    as_sources[0].clip = ac_DrownedAlt;
                as_sources[0].Play();
                break;
        }
    }
    #endregion
}
