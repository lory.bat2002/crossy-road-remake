using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class CharacterSelector : MonoBehaviour
{
    #region Variables & Properties
    bool[] unlockedChars;

    [Space(20)]
    [Header("GameObjects References")]
    [Tooltip("The reference to the gameManager")]
    [SerializeField] GameManager gameManagerRef;

    [Space(20)]
    [Header("Skins Shop GameObjects References and Parameters")]
    [Tooltip("The color for the locked characters button")]
    [SerializeField] Color32 color_LockedChar;
    [Tooltip("The color for the unlocked characters button")]
    [SerializeField] Color32 color_UnlockedChar;
    [Tooltip("The color for the selected characters button")]
    [SerializeField] Color32 color_SelectedChar;
    [Tooltip("The reference to the shop coins counter text")]
    [SerializeField] TMP_Text text_PlayerCoins;
    [Tooltip("The reference to all the selectable characters buttons")]
    [SerializeField] Button[] buttons_Chars;
    [Tooltip("The references to all locked characters overlays")]
    [SerializeField] GameObject[] lockedCharactersOverlays;
    [Tooltip("All the characters prices")]
    [SerializeField] int[] charactersPrices;
    [Tooltip("The reference to all the unlock characters buttons choices (yes/no)")]
    [SerializeField] GameObject[] unlockCharactersButtons;
    #endregion


    #region Methods
    /// <summary>
    /// Loads unlocked characters from GameInstance for shop displaying
    /// </summary>
    public void LoadUnlockedCharacters()
    {
        unlockedChars = GameInstance.UnlockedChars;

        for (int i = 0; i < buttons_Chars.Length; i++)
        {
            if (!buttons_Chars[i].gameObject.activeSelf)
                buttons_Chars[i].gameObject.SetActive(true);

            if (unlockedChars[i])
            {
                lockedCharactersOverlays[i].SetActive(false);

                if (i != GameInstance.SelectedChar)
                {
                    buttons_Chars[i].GetComponentInChildren<TMP_Text>().text = "SELECT";
                    buttons_Chars[i].GetComponent<Image>().color = color_UnlockedChar;
                }
                else
                {
                    buttons_Chars[i].GetComponentInChildren<TMP_Text>().text = "SELECTED";
                    buttons_Chars[i].GetComponent<Image>().color = color_SelectedChar;
                }
            }
            else
            {
                lockedCharactersOverlays[i].SetActive(true);
                lockedCharactersOverlays[i].GetComponentInChildren<TMP_Text>().text = charactersPrices[i].ToString();
                buttons_Chars[i].GetComponentInChildren<TMP_Text>().text = "UNLOCK";
                buttons_Chars[i].GetComponent<Image>().color = color_LockedChar;
            }
        }

        text_PlayerCoins.text = gameManagerRef.Coins.ToString();
    }


    /// <summary>
    /// Based on the selected character status, performs a different action (select/unlock) and saves the outcome
    /// </summary>
    /// <param name="characterIndex">The character index</param>
    public void SelectCharacter(int characterIndex)
    {
        if (unlockedChars[characterIndex] && characterIndex != GameInstance.SelectedChar)
        {
            buttons_Chars[GameInstance.SelectedChar].GetComponentInChildren<TMP_Text>().text = "SELECT";
            buttons_Chars[GameInstance.SelectedChar].GetComponent<Image>().color = color_UnlockedChar;
            buttons_Chars[characterIndex].GetComponentInChildren<TMP_Text>().text = "SELECTED";
            buttons_Chars[characterIndex].GetComponent<Image>().color = color_SelectedChar;

            GameInstance.SelectedChar = characterIndex;
            GameInstance.WriteSaves();

            //If selected character has a different theme, reload map theme
            if (GameInstance.CharsThemes[characterIndex] != gameManagerRef.MapGenerator.MapTheme)
            {
                gameManagerRef.MapGenerator.MapTheme = GameInstance.CharsThemes[GameInstance.SelectedChar];
                gameManagerRef.ReloadMap();
            }
            else
                gameManagerRef.PlayerRef.SwitchSkin();
        }
        else if (!unlockedChars[characterIndex])
        {
            for (int i = 0; i < buttons_Chars.Length; i++)
            {
                if (i == characterIndex)
                    buttons_Chars[i].gameObject.SetActive(false);
                else if (!buttons_Chars[i].gameObject.activeSelf)
                    buttons_Chars[i].gameObject.SetActive(true);
            }
        }
    }


    /// <summary>
    /// Stops the selected character from being unlocked
    /// </summary>
    /// <param name="characterIndex">The character index</param>
    public void DontUnlock(int characterIndex)
    {
        buttons_Chars[characterIndex].gameObject.SetActive(true);
    }


    /// <summary>
    /// Unlocks the selected character if coins are enough
    /// </summary>
    /// <param name="characterIndex">The character index</param>
    public void Unlock(int characterIndex)
    {
        if (gameManagerRef.Coins >= charactersPrices[characterIndex])
        {
            gameManagerRef.Coins -= charactersPrices[characterIndex];
            GameInstance.Coins = gameManagerRef.Coins;
            unlockedChars[characterIndex] = true;
            GameInstance.UnlockedChars = unlockedChars;
            GameInstance.WriteSaves();
            lockedCharactersOverlays[characterIndex].SetActive(false);
            buttons_Chars[characterIndex].gameObject.SetActive(true);
            buttons_Chars[characterIndex].GetComponentInChildren<TMP_Text>().text = "SELECT";
            buttons_Chars[characterIndex].GetComponent<Image>().color = color_UnlockedChar;
            text_PlayerCoins.text = gameManagerRef.Coins.ToString();
        }
        else
            DontUnlock(characterIndex);
    }
    #endregion
}
