using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


public static class GameInstance
{
    #region Variables & Properties
    static int bestDistance;
    static int coins;
    static bool[] unlockedChars;
    static int selectedChar;
    static int[] charsThemes;

    public static int BestDistance { get { return bestDistance; } set { bestDistance = value; } }
    public static int Coins { get { return coins; } set { coins = value; } }
    public static bool[] UnlockedChars { get { return unlockedChars; } set { unlockedChars = value; } }
    public static int SelectedChar { get { return selectedChar; } set { selectedChar = value; } }
    public static int[] CharsThemes { get { return charsThemes; } set { charsThemes = value; } }
    #endregion


    #region Methods
    /// <summary>
    /// Loads all save data from very basic .txt file (to enable other team members for easy save manipulation during testing)
    /// </summary>
    public static void LoadSaves()
    {
        StreamReader reader = new StreamReader(Application.streamingAssetsPath + "/Save.txt");

        bestDistance = int.Parse(reader.ReadLine());
        coins = int.Parse(reader.ReadLine());

        string[] unlockedCharsStrings = reader.ReadLine().Split(' ');
        unlockedChars = new bool[unlockedCharsStrings.Length];

        for (int i = 0; i < unlockedCharsStrings.Length; i++)
        {
            if (int.Parse(unlockedCharsStrings[i]) > 0)
                unlockedChars[i] = true;
            else
                unlockedChars[i] = false;
        }

        selectedChar = int.Parse(reader.ReadLine());

        string[] charsThemesStrings = reader.ReadLine().Split(' ');
        charsThemes = new int[charsThemesStrings.Length];

        for (int i = 0; i < charsThemesStrings.Length; i++)
            charsThemes[i] = int.Parse(charsThemesStrings[i]);

        reader.Close();
    }


    /// <summary>
    /// Saves all save data to the same .txt file
    /// </summary>
    public static void WriteSaves()
    {
        StreamWriter writer = new StreamWriter(Application.streamingAssetsPath + "/Save.txt", false);

        writer.WriteLine(bestDistance);
        writer.WriteLine(coins);

        string tmpSaveString = "";

        for (int i = 0; i < unlockedChars.Length; i++)
        {
            if (unlockedChars[i])
                tmpSaveString += "1 ";
            else
                tmpSaveString += "0 ";
        }

        tmpSaveString = tmpSaveString.Remove(tmpSaveString.Length-1);

        writer.WriteLine(tmpSaveString);
        writer.WriteLine(selectedChar);

        tmpSaveString = "";
        for (int i = 0; i < charsThemes.Length; i++)
                tmpSaveString += charsThemes[i] + " ";

        tmpSaveString = tmpSaveString.Remove(tmpSaveString.Length - 1);
        writer.WriteLine(tmpSaveString);

        writer.Close();
    }
    #endregion
}
