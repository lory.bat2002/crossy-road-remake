using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LaternFishMovement : MonoBehaviour
{
    #region Variables & Properties
    Vector3 startingPosition;
    float timer;
    bool playingAnimation;

    [Space(20)]
    [Header("Movement parameters")]
    [Tooltip("The sinusoidal animation looping time (in seconds)")]
    [SerializeField] float animationTime;
    [Tooltip("The animation y height variation")]
    [SerializeField] float heightVariation;
    #endregion


    #region Mono
    //Initializes the lantern fish once instantiated
    private void Start()
    {
        StartCoroutine(InitializeLanternFish());
    }


    //Basic sinusoidal animation
    private void Update()
    {
        if (!playingAnimation) return;

        timer += Time.deltaTime;

        transform.position = startingPosition + new Vector3(0f, heightVariation * Mathf.Sin(2 * Mathf.PI / animationTime * timer), 0f);

        if (timer >= animationTime)
            timer = 0f;
    }
    #endregion


    #region Methods
    /// <summary>
    /// Initializes lantern fish (after random async pause) by also storing its starting position for the sinusoidal animation
    /// </summary>
    /// <returns></returns>
    IEnumerator InitializeLanternFish()
    {
        startingPosition = transform.position;
        yield return new WaitForSeconds(Random.Range(.1f, 1.5f));
        playingAnimation = true;
    }
    #endregion
}
