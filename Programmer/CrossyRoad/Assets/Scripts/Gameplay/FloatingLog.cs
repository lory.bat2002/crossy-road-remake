using System.Collections;
using Unity.VisualScripting;
using UnityEngine;


public class FloatingLog : MonoBehaviour
{
    #region Variables & Properties
    AudioSource as_source;
    Transform logTransform;
    float logStartingHeight;

    bool playAnimation;
    float timer;

    [Space(20)]
    [Header("Log Parameters")]
    [Tooltip("How fast should the log be going")]
    [SerializeField] float speed;
    [Tooltip("After how much time should the log be automatically destroyed")]
    [SerializeField] float autodestructionTimer;

    [Space(20)]
    [Header("Animation Parameters")]
    [Tooltip("How many seconds will water lily animation last for")]
    [SerializeField] float logAnimationTime;
    [Tooltip("How lower will water lily go after collision")]
    [SerializeField] float logHeightChange;
    
    [Space(20)]
    [Header("Log References")]
    [Tooltip("Reference to the log anchors")]
    [SerializeField] Transform[] anchors;

    public float Speed { get { return speed; } set { speed = value; } }
    #endregion


    #region Mono
    //Gets all required references and the starting y position to later apply sinusoidal animations
    private void Start()
    {
        StartCoroutine(Autodestruction());
        as_source = GetComponent<AudioSource>();
        logTransform = GetComponentInChildren<Transform>();
        logStartingHeight = logTransform.position.y;
    }


    //Basic customizable sinusoidal animation played each time its stepped on
    private void Update()
    {
        transform.position += (transform.forward * speed * Time.deltaTime);

        if (!playAnimation) return;

        timer += Time.deltaTime;

        logTransform.position = new Vector3(transform.position.x, logStartingHeight - logHeightChange * Mathf.Sin(Mathf.PI / logAnimationTime * (timer)), transform.position.z);

        if (timer >= logAnimationTime)
        {
            playAnimation = false;
            timer = 0f;
        }
    }
    #endregion


    #region Methods
    /// <summary>
    /// Automatically destroys object after <audestructionTimer> seconds
    /// </summary>
    /// <returns></returns>
    IEnumerator Autodestruction()
    {
        yield return new WaitForSeconds(autodestructionTimer);
        Destroy(gameObject);
    }


    /// <summary>
    /// Starts sinusoidal animation and plays creaking sound
    /// </summary>
    public void StartAnimation()
    {
        if (!playAnimation)
            playAnimation = true;
        else
            timer = 0f;

        as_source.pitch = Random.Range(.9f, 1.1f);
        as_source.Play();
    }


    /// <summary>
    /// Returns the closest anchor point relative to the new predicted player position
    /// </summary>
    /// <param name="playerPosition">The predicted player position</param>
    /// <returns></returns>
    public Vector3 GetClosestAnchorPointPosition(Vector3 playerPosition)
    {
        int anchorIndex = 0;
        float distance;
        float minDistance = float.MaxValue;

        //For each anchor point, compute distance and return closest anchor position
        for (int i = 0; i < anchors.Length; i++)
        {
            distance = Mathf.Pow(anchors[i].transform.position.x - playerPosition.x, 2) + Mathf.Pow(anchors[i].transform.position.z - playerPosition.z, 2);
            if (distance < minDistance)
            {
                minDistance = distance;
                anchorIndex = i;
            }
        }

        return anchors[anchorIndex].position;
    }
    #endregion
}
