using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WaterLily : MonoBehaviour
{
    #region Variables & Properties
    AudioSource as_source;
    Player playerRef;
    Transform lilypadTransform;
    Vector3 playerStartingPosition;
    Vector3 lilipadStartingPosition;

    bool playAnimation;
    float timer;

    [Space(20)]
    [Header("Animation Parameters")]
    [Tooltip("How many seconds will water lily animation last for")]
    [SerializeField] float waterLilyAnimationTime;
    [Tooltip("How lower will water lily go after collision")]
    [SerializeField] float waterLilyHeightChange;
    #endregion


    #region Mono
    //Gets required components and starting position for sinusoidal animation
    private void Start()
    {
        as_source = GetComponent<AudioSource>();
        lilypadTransform = GetComponentInChildren<Transform>();
        lilipadStartingPosition = lilypadTransform.position;
    }


    //Basic customizable sinusoidal animation
    private void Update()
    {
        if (!playAnimation) return;

        timer += Time.deltaTime;

        lilypadTransform.position = lilipadStartingPosition + new Vector3(0f, -waterLilyHeightChange * Mathf.Sin(Mathf.PI / waterLilyAnimationTime * (timer)), 0f);
        if (playerRef != null && playerRef.GettingCarried)
            playerRef.transform.position = playerStartingPosition + new Vector3(0f, -waterLilyHeightChange * Mathf.Sin(Mathf.PI / waterLilyAnimationTime * (timer)), 0f);

        if (timer >= waterLilyAnimationTime)
        {
            playAnimation = false;
            timer = 0f;
        }
    }


    //Once stepped on play sinusoidal animation a play sinking sound
    private void OnCollisionEnter(Collision collision)
    {
        playerRef = collision.gameObject.GetComponent<Player>();
        if (!playerRef.GettingCarried)
            playerRef.GettingCarried = true;
        playerStartingPosition = playerRef.transform.position;
        playAnimation = true;

        as_source.pitch = Random.Range(.9f, 1.1f);
        as_source.Play();
    }


    //Once the player leaves the pad, remove its reference
    private void OnCollisionExit(Collision collision)
    {
        playerRef = null;
    }
    #endregion
}
