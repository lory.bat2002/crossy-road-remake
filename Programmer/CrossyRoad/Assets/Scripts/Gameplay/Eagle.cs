using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Eagle : MonoBehaviour
{
    #region Variables & Properties
    GameObject playerGameObject;
    Vector3 spawnPosition;
    Vector3 targetPosition;

    float timer;
    bool chasing;
    bool fleeing;

    [Space(20)]
    [Header("Eagle Parameters")]
    [Tooltip("How much time will take for the eagle to be automatically destroyed once instantiated")]
    [SerializeField] float autodestructionTimer;
    [Tooltip("How long will the chasing animation be")]
    [SerializeField] float chasingAnimationTime;
    #endregion


    #region Mono
    //Starts the autodestruction sequence
    private void Start()
    {
        StartCoroutine(Autodestruction());
    }


    //Handles the basic chasing and fleeing animations
    private void Update()
    {
        if (chasing && timer < chasingAnimationTime)
        {
            //Chase
            timer += Time.deltaTime;
            transform.position = Vector3.Lerp(spawnPosition, targetPosition, timer/chasingAnimationTime);
        }
        else if (timer > chasingAnimationTime && chasing)
        {
            //Parent the player and start fleeing
            playerGameObject.transform.parent = transform;
            spawnPosition = transform.position;
            targetPosition = targetPosition + Vector3.up * 5f - Vector3.forward * 20f;
            transform.rotation = Quaternion.LookRotation(targetPosition - transform.position);
            chasing = false;
            fleeing = true;
            timer = 0;
        }
        else if (fleeing && timer < chasingAnimationTime)
        {
            //Flee
            timer += Time.deltaTime;
            transform.position = Vector3.Lerp(spawnPosition, targetPosition, timer / chasingAnimationTime);
        }
    }
    #endregion


    #region Methods
    /// <summary>
    /// Destroys the eagle after <autodestructionTimer> seconds
    /// </summary>
    /// <returns></returns>
    IEnumerator Autodestruction()
    {
        yield return new WaitForSeconds(autodestructionTimer);
        Destroy(gameObject);
    }


    /// <summary>
    /// Initializes the eagle and its chasing animation once instantiated
    /// </summary>
    /// <param name="target">The target to seek</param>
    public void StartChasing(GameObject target)
    {
        spawnPosition = transform.position;
        playerGameObject = target;
        targetPosition = target.transform.position + Vector3.up * .5f;
        transform.rotation = Quaternion.LookRotation(targetPosition - transform.position);
        chasing = true;
    }
    #endregion
}
