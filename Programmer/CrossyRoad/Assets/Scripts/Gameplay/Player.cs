using System.Collections;
using UnityEngine;


public enum GameOverType
{
    OUT_OF_BOUNDS,
    OUT_OF_CAMERA,
    RUN_OVER,
    RUN_OVER_FAST,
    DROWNED
}


public class Player : MonoBehaviour
{
    #region Variables & Properties
    GameManager gameManagerRef;
    AudioSource as_source;
    Rigidbody rb;
    MeshFilter mf;
    Renderer rdr;
    FloatingLog logRef;
    RaycastHit hit;
    RaycastHit terrainHit;
    Vector3 nextDir;
    Vector3 curPosition;

    bool canMove;
    bool gettingCarried;
    float xAxisValue;
    float zAxisValue;
    float terrainHeight = .6f;
    int selectedCharacterSkin;

    [Space(20)]
    [Header("Player Parameters")]
    [Tooltip("Player movement speed")]
    [SerializeField] float movingSpeed;
    [Tooltip("Player movement speed")]
    [SerializeField] float rotationSpeed;
    [Tooltip("Player jump strength")]
    [SerializeField] float jumpForce;
    [Tooltip("How long will jump last for before being pushed back to the ground")]
    [SerializeField] float jumpWindowTime;

    [Space(20)]
    [Header("Collision Parameters")]
    [Tooltip("Which kind of obstacles should the player be looking out for")]
    [SerializeField] LayerMask obstaclesRaycastChannel;
    [Tooltip("Which layer should raycast be tracing for height detection")]
    [SerializeField] LayerMask terrainRaycastChannel;

    [Space(20)]
    [Header("Player Skins")]
    [Tooltip("All the playable meshes")]
    [SerializeField] Mesh[] mf_SkinsMeshFilters;
    [Tooltip("All the playable materials")]
    [SerializeField] Material[] mf_SkinsMaterials;

    public GameManager GameManager { set { gameManagerRef = value; } }
    public bool CanMove { set { canMove = value; } }
    public bool GettingCarried { get { return gettingCarried; } set { gettingCarried = value; } }
    public float GetVelocitySquaredMagnitude { get { return Mathf.Round(rb.velocity.sqrMagnitude); } }
    #endregion


    #region Mono
    //Gets required components and switches skin to selected one
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        mf = GetComponentInChildren<MeshFilter>();
        rdr = GetComponentInChildren<Renderer>();
        curPosition = transform.position;

        SwitchSkin();
    }


    //Handles character movement
    private void Update()
    {
        if (!canMove) return;

        //Calculate the next predicted position
        Vector3 nextPosition = new Vector3(curPosition.x, terrainHeight, curPosition.z) + nextDir;

        //If a new position has been identified, move and rotate towards it
        if (transform.position != nextPosition && !gettingCarried)
        {
            transform.position = Vector3.MoveTowards(transform.position, nextPosition, movingSpeed * Time.deltaTime);
            if (nextDir != Vector3.zero)
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(nextDir), rotationSpeed * Time.deltaTime);
            transform.localScale = new Vector3(transform.localScale.x, nextDir.z != 0 ? (-.3f*Mathf.Cos(2f*(nextPosition.z - transform.position.z + Mathf.PI/2f)) + .7f) : (-.3f * Mathf.Cos(2f * (nextPosition.x - transform.position.x + Mathf.PI / 2f)) + .7f), transform.localScale.z);
        }    
        else if (rb.velocity == Vector3.zero || gettingCarried)
        {
            //If nextDir hasn't already been reset, reset it
            if (nextDir != Vector3.zero)
            {
                nextDir = Vector3.zero;
                curPosition = transform.position;
            }

            curPosition.x = Mathf.RoundToInt(transform.position.x);
            curPosition.z = Mathf.RoundToInt(transform.position.z);

            //WASD movement (+space/click movement)
            if (zAxisValue != 0)
                zAxisValue = 0;
            if (xAxisValue != 0)
                xAxisValue = 0;

            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.Space))
                zAxisValue = 1f;
            else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
                zAxisValue = -1f;
            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
                xAxisValue = 1f;
            else if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
                xAxisValue = -1f;
            
            //Check for obstacles, otherwise set next direction and start jumping
            if (zAxisValue != 0 && (zAxisValue == 1 ? !CheckForHit(Vector3.forward) : !CheckForHit(Vector3.back)))
            {
                if (gettingCarried)
                {
                    gettingCarried = false;
                    transform.position = new Vector3(transform.position.x, terrainHeight, transform.position.z);
                }

                nextDir.z = zAxisValue;
                StartCoroutine(SimulateJump());
            }
            else if (xAxisValue != 0 && (xAxisValue == 1 ? !CheckForHit(Vector3.right) : !CheckForHit(Vector3.left)))
            {
                if (gettingCarried)
                {
                    gettingCarried = false;
                    transform.position = new Vector3(transform.position.x, terrainHeight, transform.position.z);
                }
                
                nextDir.x = xAxisValue;
                StartCoroutine(SimulateJump());
            }
        }
    }
    #endregion


    #region Methods
    /// <summary>
    /// Loads the selected character skin
    /// </summary>
    public void SwitchSkin()
    {
        selectedCharacterSkin = GameInstance.SelectedChar;
        mf.mesh = mf_SkinsMeshFilters[selectedCharacterSkin];
        rdr.material = mf_SkinsMaterials[selectedCharacterSkin];
    }


    /// <summary>
    /// Simulates a jumping animation (by also calculating terrain height) and sends information to game manager about the next player location
    /// </summary>
    /// <returns></returns>
    IEnumerator SimulateJump()
    {
        GetTerrainHeight();

        //GameManager checks new player position to update game status
        gameManagerRef.CheckNewPlayerPosition(transform.position + nextDir);

        if (!gettingCarried)
        {
            as_source.Play();

            if (rb != null)
                rb.AddForce(0f, jumpForce, 0f);
            yield return new WaitForSeconds(jumpWindowTime);
            if (rb != null)
                rb.AddForce(0f, -jumpForce, 0f);
        }
    }


    /// <summary>
    /// Checks for raycast collision in given direction
    /// </summary>
    /// <param name="direction">Which direction to look towards</param>
    /// <returns></returns>
    bool CheckForHit(Vector3 direction)
    {
        if (Physics.Raycast(transform.position, direction, 1f, obstaclesRaycastChannel))
            return true;
        return false;
    }


    /// <summary>
    /// Ends the game by calling the gameManager and performs different actions on the player side based on GameOverType
    /// </summary>
    /// <param name="type"></param>
    public void GameOver(GameOverType type)
    {
        StartCoroutine(gameManagerRef.GameOver(type));

        switch (type)
        {
            case GameOverType.RUN_OVER:
                canMove = false;
                transform.position = new Vector3(transform.position.x, terrainHeight - transform.localScale.y / 2 + .05f, transform.position.z);
                transform.localScale = new Vector3(1.2f, .1f, 1.2f);
                Destroy(GetComponent<Collider>());
                Destroy(rb);
                break;

            case GameOverType.RUN_OVER_FAST: 
                canMove = false;
                transform.localScale = new Vector3(1.2f, 1.2f, .1f);
                if (zAxisValue > 0)
                    transform.rotation = Quaternion.identity;
                else
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                Destroy(GetComponent<Collider>());
                Destroy(rb);
                break;

            case GameOverType.OUT_OF_BOUNDS:
                canMove = false;
                Destroy(GetComponent<Collider>());
                Destroy(rb);
                break;

            case GameOverType.OUT_OF_CAMERA:
                canMove = false;
                Destroy(GetComponent<Collider>());
                Destroy(rb);
                transform.position = new Vector3(transform.position.x, .5f, transform.position.z);
                transform.localScale = Vector3.one;
                break;
        }
    }


    /// <summary>
    /// Calculates terrain height (and handles floatingLog movement)
    /// </summary>
    void GetTerrainHeight()
    {
        if (Physics.Raycast(new Vector3(transform.position.x, .5f, transform.position.z) + nextDir, Vector3.down * 2f, out terrainHit, terrainRaycastChannel))
        {
            //Log movement checks
            if (terrainHit.collider.GetComponent<FloatingLog>() != null)
                MovePlayerOnLog();
            else
            {
                Physics.Raycast(new Vector3(transform.position.x, .5f, transform.position.z) + nextDir + Vector3.right * .3f, Vector3.down * .5f, out terrainHit, terrainRaycastChannel);
                if (terrainHit.collider.GetComponent<FloatingLog>() != null)
                    MovePlayerOnLog();
                else
                {
                    Physics.Raycast(new Vector3(transform.position.x, .5f, transform.position.z) + nextDir + Vector3.left * .3f, Vector3.down * .5f, out terrainHit, terrainRaycastChannel);
                    if (terrainHit.collider.GetComponent<FloatingLog>() != null)
                        MovePlayerOnLog();
                    else if (transform.parent != null)
                    {
                        transform.parent = null;
                        gettingCarried = false;
                    }
                }
            }

            terrainHeight = transform.localScale.y - terrainHit.distance;
        }
    }


    /// <summary>
    /// Nerfed floatingLog movement to avoid animation bugs
    /// </summary>
    void MovePlayerOnLog()
    {
        if (!gettingCarried)
            gettingCarried = true;

        if (transform.parent == null || logRef != terrainHit.collider.GetComponent<FloatingLog>())
        {
            logRef = terrainHit.collider.GetComponent<FloatingLog>();
            transform.parent = logRef.transform;
        }

        //Play log animation
        logRef.StartAnimation();

        nextDir = logRef.GetClosestAnchorPointPosition(transform.position + nextDir);
        nextDir.y = terrainHeight;
        transform.position = nextDir;
        transform.rotation = Quaternion.identity;
        nextDir = Vector3.zero;
    }


    /// <summary>
    /// Sets the player jumping sound based on the AudioClip passed by the gameManager
    /// </summary>
    /// <param name="clip"></param>
    public void SetJumpSound(AudioClip clip)
    {
        as_source = GetComponent<AudioSource>();
        as_source.clip = clip;
    }
    #endregion
}
