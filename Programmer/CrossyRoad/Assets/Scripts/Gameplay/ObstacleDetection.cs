using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ObstacleDetection : MonoBehaviour
{
    #region Variables & Properties
    AudioSource as_source;

    bool canHonk = true;

    [Space(20)]
    [Header("Detection Parameters")]
    [Tooltip("How much minimum time will the car need to wait between each honk")]
    [SerializeField] float honkingCooldown;
    #endregion


    #region Mono
    //Gets audio source for playing honking sound
    private void Start()
    {
        as_source = GetComponent<AudioSource>();
    }


    //If the player enters the warning trigger, start honking
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>() != null)
        {
            if (canHonk)
                StartCoroutine(Honk());
        }
    }
    #endregion


    #region Methods
    /// <summary>
    /// Honking cooldown
    /// </summary>
    /// <returns></returns>
    IEnumerator Honk()
    {
        canHonk = false;
        as_source.Play();
        yield return new WaitForSeconds(honkingCooldown);
        canHonk = true;
    }
    #endregion
}
