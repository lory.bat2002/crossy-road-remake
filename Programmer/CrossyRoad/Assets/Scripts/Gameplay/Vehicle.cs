using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Vehicle : MonoBehaviour
{
    #region Variables & Properties
    [Header("Vehicle Parameters")]
    [Tooltip("How fast should the vehicle be going")]
    [SerializeField] protected float speed;
    [Tooltip("After how much time should the vehicle be automatically destroyed")]
    [SerializeField] protected float autodestructionTimer;

    public float Speed { set { speed = value; } }
    #endregion


    #region Mono
    //Starts autodestruction coroutine
    private void Start()
    {
        StartCoroutine(Autodestruction());
    }


    //Basic vehicle movement
    private void Update()
    {
        transform.position += (transform.forward * speed * Time.deltaTime);
    }


    //On player collision, game over
    private void OnTriggerEnter(Collider collider)
    {
        if (speed <= 25 && (Mathf.Abs(Vector3.Dot(transform.forward, (collider.gameObject.transform.position - transform.position).normalized)) > .7f))
            collider.GetComponent<Player>().GameOver(GameOverType.RUN_OVER);
        else
        {
            collider.gameObject.transform.parent = transform;
            collider.GetComponent<Player>().GameOver(GameOverType.RUN_OVER_FAST);
        }
            
    }
    #endregion


    #region Methods
    /// <summary>
    /// Automatically destroys object after <audestructionTimer> seconds
    /// </summary>
    /// <returns></returns>
    protected IEnumerator Autodestruction()
    {
        yield return new WaitForSeconds(autodestructionTimer);
        Destroy(gameObject);
    }
    #endregion
}
