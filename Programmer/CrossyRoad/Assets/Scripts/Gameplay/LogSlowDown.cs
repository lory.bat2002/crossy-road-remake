using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LogSlowDown : MonoBehaviour
{
    #region Variables & Properties
    Player playerRef;

    [Space(20)]
    [Header("Slow Down Parameters")]
    [Tooltip("Log slow down multiplier")]
    [SerializeField] float slowDownMultiplier;
    #endregion


    #region Mono
    //If a log enters in the zone, slow it down
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.GetComponent<FloatingLog>() != null)
            collider.gameObject.GetComponent<FloatingLog>().Speed /= slowDownMultiplier;
    }


    //If a log exits the zone, speed it up
    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.GetComponent<FloatingLog>() != null)
            collider.gameObject.GetComponent<FloatingLog>().Speed *= slowDownMultiplier;
    }
    #endregion
}
