using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PoliceCar : Vehicle
{
    #region Variables & Properties
    WaitForSeconds blinkingWait;

    [Tooltip("How fast should the lights be blinking (seconds/switch)")]
    [SerializeField] float blinkingFrequency;

    [Space(20)]
    [Header("Vehicle References")]
    [Tooltip("Blinking lights references")]
    [SerializeField] Light[] lights;
    #endregion


    #region Mono
    //Initializes the autodestruction and blinking lights coroutines (by also loading the blinking frequency)
    private void Start()
    {
        blinkingWait = new WaitForSeconds(blinkingFrequency);
        StartCoroutine(Autodestruction());
        StartCoroutine(BlinkingLights());
    }


    //Very basic straight movement
    private void Update()
    {
        transform.position += (transform.forward * speed * Time.deltaTime);
    }


    //If a gameobject enters the policeCar trigger it gets run over
    private void OnTriggerEnter(Collider collider)
    {
        collider.GetComponent<Player>().GameOver(GameOverType.RUN_OVER);
    }
    #endregion


    #region Methods
    /// <summary>
    /// Basic blinking lights animation
    /// </summary>
    /// <returns></returns>
    IEnumerator BlinkingLights()
    {
        while (true)
        {
            lights[0].enabled = true;
            lights[1].enabled = false;
            yield return blinkingWait;
            lights[0].enabled = false;
            lights[1].enabled = true;
            yield return blinkingWait;
        }
    }
    #endregion
}
