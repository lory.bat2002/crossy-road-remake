using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Coin : MonoBehaviour
{
    #region Variables & Properties
    GameManager gameManagerRef;
    Vector3 startingPosition;

    float timer;

    [Space(20)]
    [Header("Coin Parameters")]
    [Tooltip("How many seconds will the animation last for")]
    [SerializeField] float animationTime;
    [Tooltip("After how many seconds should coin start bouncing")]
    [SerializeField] float animationStartTime;
    [Tooltip("How many seconds between each jumping animation")]
    [SerializeField] float animationLoopTime;
    [Tooltip("How high will the coin jump during the animation")]
    [SerializeField] float animationJumpHeight;

    public GameManager GameManager { set { gameManagerRef = value; } }
    #endregion


    #region Mono
    //Saves the starting position for the sinusoidal animation
    private void Start()
    {
        startingPosition = transform.position;
    }


    //Simple customizable sinusoidal animation to make the coin jump from time to time
    private void Update()
    {
        timer += Time.deltaTime;

        if (timer >= animationLoopTime)
            timer = 0;

        if (timer >= animationStartTime && timer < animationStartTime + animationTime)
            transform.position = startingPosition + new Vector3(0f, animationJumpHeight * Mathf.Sin(Mathf.PI / animationTime * (timer - animationStartTime)), 0f);
    }


    //Trigger for simply collecting the coins
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Player>() != null)
        {
            gameManagerRef.AddCoin();
            Destroy(gameObject);
        }
    }
    #endregion
}
