using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class GameUI : MonoBehaviour
{
    //This script is how NOT to handle a UI, it was done in a hurry (it just works, .cit)

    #region Variables & Properties
    [Space(20)]
    [Header("Scene GameObjects References")]
    [Tooltip("The gameManager gameobject reference")]
    [SerializeField] GameManager gameManagerRef;
    [Tooltip("The characterSelector gameobject reference")]
    [SerializeField] CharacterSelector characterSelectorRef;

    [Space(20)]
    [Header("UI GameObjects References")]
    [Tooltip("The ui gameobject containing game filters")]
    [SerializeField] GameObject ui_Filters;
    [Tooltip("The ui gameobject containing the game main menu")]
    [SerializeField] GameObject ui_Menu;
    [Tooltip("The ui gameobject containing the game ui")]
    [SerializeField] GameObject ui_Game;
    [Tooltip("The ui gameobject containing the game over ui")]
    [SerializeField] GameObject ui_GameOver;
    [Tooltip("The ui gameobject containing the skin shop ui")]
    [SerializeField] GameObject ui_CharacterSelection;

    [Space(20)]
    [Header("GameUI and GameOver Elements References")]
    [Tooltip("The gameUI score distance text reference")]
    [SerializeField] TMP_Text text_DistanceScore;
    [Tooltip("The gameUI coin text reference")]
    [SerializeField] TMP_Text text_Coins;
    [Tooltip("The gameOver score distance text reference")]
    [SerializeField] TMP_Text text_Score;
    [Tooltip("The gameOver distance highscore text reference")]
    [SerializeField] TMP_Text text_HighScore;
    #endregion


    #region Methods
    /// <summary>
    /// Updates max player distance UI
    /// </summary>
    /// <param name="newDistance">New player distance</param>
    public void UpdateDistanceScore(int newDistance)
    {
        text_DistanceScore.text = newDistance.ToString();
    }


    /// <summary>
    /// Updates the player coin count
    /// </summary>
    /// <param name="newCoinCount"></param>
    public void UpdateCoins(int newCoinCount)
    {
        text_Coins.text = newCoinCount.ToString();
    }


    /// <summary>
    /// Loads filter based on the requested <filterIndex> value
    /// </summary>
    /// <param name="filterIndex"></param>
    public void LoadFilters(int filterIndex)
    {
        switch (filterIndex)
        {
            case 0:
                ui_Filters.SetActive(false);
                break;

           case 1:
                ui_Filters.SetActive(true);
                break;
        }
    }


    /// <summary>
    /// Switches from gameOverUI to mainMenuUI
    /// </summary>
    public void SwitchToMenuUIAndReloadGame()
    {
        ui_GameOver.SetActive(false);
        ui_Menu.SetActive(true);
        gameManagerRef.ReloadMap();
    }


    /// <summary>
    /// Switches from skinsShopUI to mainMenuUI
    /// </summary>
    public void SwitchToMenuUI()
    {
        ui_CharacterSelection.SetActive(false);
        ui_Menu.SetActive(true);
    }


    /// <summary>
    /// Switches from mainMenuUI to skinsShopUI
    /// </summary>
    public void SwitchToCharacterSelectionUI()
    {
        ui_Menu.SetActive(false);
        ui_CharacterSelection.SetActive(true);
        characterSelectorRef.LoadUnlockedCharacters();
    }


    /// <summary>
    /// Switches from mainMenuUI to gameUI
    /// </summary>
    public void SwitchToGameUI()
    {
        ui_Menu.SetActive(false);
        ui_Game.SetActive(true);
        gameManagerRef.StartGame();
    }


    /// <summary>
    /// Switches from gameUI to gameOverUI
    /// </summary>
    /// <param name="score">The current round distance score</param>
    /// <param name="highestScore">The highest player distance score</param>
    public void SwitchToGameOverUI(int score, int highestScore)
    {
        ui_Game.SetActive(false);
        ui_GameOver.SetActive(true);
        text_Score.text = "Score: " + score.ToString();
        text_HighScore.text = "Highscore: " + highestScore.ToString();
    }


    /// <summary>
    /// Quits game
    /// </summary>
    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }
    #endregion
}
