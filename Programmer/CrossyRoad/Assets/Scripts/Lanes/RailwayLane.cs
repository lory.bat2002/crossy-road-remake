using System.Collections;
using UnityEngine;


public class RailwayLane : Lane
{
    #region Variables & Properties
    Transform vehicleSpawn;
    AudioSource as_source;

    float timer;

    [Space(20)]
    [Header("Lane Parameters")]
    [Tooltip("The minimum time between vehicles spawns")]
    [SerializeField] float minSpawnTime;
    [Tooltip("The maximum time between vehicles spawns")]
    [SerializeField] float maxSpawnTime;
    [Tooltip("How many seconds will light turn on before the spawning train")]
    [SerializeField] float lightPredictionTime;
    [Tooltip("How many seconds will light stay on for")]
    [SerializeField] float lightOnDuration;

    [Space(20)]
    [Header("Lane References")]
    [Tooltip("References to the train ringing line")]
    [SerializeField] Light trainLight;
    [Tooltip("References to spawn objects used by the lane")]
    [SerializeField] Transform[] vehicleSpawns;

    [Space(20)]
    [Header("Spawnable Lane Items")]
    [Tooltip("Which item can be spawned in this lane")]
    [SerializeField] GameObject spawnableVehicle;
    #endregion


    #region Methods
    //Initializes railway lane by selecting the spawn orientation and starting 
    public void InitializeLane()
    {
        as_source = GetComponent<AudioSource>();

        int probability = Random.Range(0, 2);
        if (probability == 0)
            vehicleSpawn = vehicleSpawns[0];
        else
            vehicleSpawn = vehicleSpawns[1];

        StartCoroutine(SpawnVehicle());
    }


    /// <summary>
    /// Spawns the trains by syncronizing the lane ringing lights
    /// </summary>
    /// <returns></returns>
    IEnumerator SpawnVehicle()
    {
        timer = Random.Range(minSpawnTime, maxSpawnTime);
        yield return new WaitForSeconds(timer - lightPredictionTime);
        trainLight.gameObject.SetActive(true);
        as_source.Play();
        yield return new WaitForSeconds(lightPredictionTime);
        Instantiate(spawnableVehicle, vehicleSpawn.position, vehicleSpawn.rotation);
        yield return new WaitForSeconds(lightOnDuration - lightPredictionTime);
        trainLight.gameObject.SetActive(false);

        StartCoroutine(SpawnVehicle());
    }


    /// <summary>
    /// Adds gameObject to lane items list
    /// </summary>
    /// <param name="gameObject">The gameObject to be added</param>
    public void AddItem(GameObject gameObject)
    {
        laneItems.Add(gameObject);
    }


    /// <summary>
    /// Destroyes lane and all its items
    /// </summary>
    public void DestroyLane()
    {
        if (laneItems != null)
        {
            for (int i = 0; i < laneItems.Count; i++)
                if (laneItems[i] != null)
                    Destroy(laneItems[i]);
        }

        Destroy(gameObject);
    }
    #endregion
}
