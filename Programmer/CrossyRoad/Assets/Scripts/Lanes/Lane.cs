using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct LaneItem
{
    public GameObject item;
    public int probability;
}


public class Lane : MonoBehaviour
{
    //All lanes types where supposed to inherit from this class, but all the work was done before the "Inheritance programming lesson", so everything has been nerfed down

    #region Variables & Properties
    public LaneType type;
    protected List<GameObject> laneItems = new List<GameObject>();
    #endregion


    #region Methods
    public void AddItem(GameObject gameObject)
    {
        laneItems.Add(gameObject);
    }
    #endregion
}
