using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ObstaclesDistribution
{
    NORMAL,
    BACKSPAWN,
    SPAWN
}


public class GrassLane : Lane
{
    #region Variables & Properties
    [Space(20)]
    [Header("Spawn Parameters")]
    [Tooltip("How many items will be considered for spawn placement (starting from top)")]
    [SerializeField] int spawnItemsRange;

    [Space(20)]
    [Header("Spawnable Lane Items")]
    [Tooltip("Which items can be spawned in this lane (and their spawn rate)")]
    [SerializeField] LaneItem[] laneSpawnableItems;
    #endregion


    #region Methods
    /// <summary>
    /// Initializes grass lane by placing obstances based on given probabilities and distribution type
    /// </summary>
    /// <param name="clearPaths">The bool array containing all obstacles forbidden placement positions</param>
    /// <param name="internalSpawnProbability">The probability of spawing obstacles inside the playable area</param>
    /// <param name="externalSpawnProbability">The probability of spawing obstacles outside the playable area</param>
    /// <param name="distribution">The obstacles distribution pattern</param>
    public void InitializeLane(bool[] clearPaths, int internalSpawnProbability, int externalSpawnProbability, ObstaclesDistribution distribution)
    {
        int probability;

        for (int i = -(int)transform.localScale.x * 10 / 2 + 1; i < (int)transform.localScale.x * 10 / 2; i++)
        {
            switch (distribution)
            {
                case ObstaclesDistribution.NORMAL:
                    probability = Random.Range(1, 101);

                    //If external spawning, else internal spawning
                    if (i < -clearPaths.Length / 2 || i > clearPaths.Length / 2)
                    {
                        if (probability <= externalSpawnProbability)
                            laneItems.Add(Instantiate(laneSpawnableItems[PickSpawnedItem()].item, new Vector3(i, 0f, transform.position.z), Quaternion.Euler(0f, Random.Range(0, 4) * 90f, 0f)));
                    }    
                    else if (!clearPaths[i + clearPaths.Length / 2] && probability <= internalSpawnProbability)
                        laneItems.Add(Instantiate(laneSpawnableItems[PickSpawnedItem()].item, new Vector3(i, 0f, transform.position.z), Quaternion.Euler(0f, Random.Range(0, 4) * 90f, 0f)));
                    break;

                case ObstaclesDistribution.BACKSPAWN:
                    laneItems.Add(Instantiate(laneSpawnableItems[Random.Range(0, spawnItemsRange)].item, new Vector3(i, 0f, transform.position.z), Quaternion.identity));
                    break; 
                
                case ObstaclesDistribution.SPAWN:
                    if (i < -clearPaths.Length / 2 + 1 || i > clearPaths.Length / 2 - 1)
                        laneItems.Add(Instantiate(laneSpawnableItems[Random.Range(0, spawnItemsRange)].item, new Vector3(i, 0f, transform.position.z), Quaternion.identity));
                    break;
            } 
        }   
    }


    /// <summary>
    /// Selects the obstacle to be spawned based on weighted probabilities
    /// </summary>
    /// <returns></returns>
    int PickSpawnedItem()
    {
        int index = 0;
        float random = UnityEngine.Random.value;

        while (random > 0)
        {
            random -= (float)laneSpawnableItems[index].probability / 100;
            index++;
        }
        index--;

        return index;
    }


    /// <summary>
    /// Adds gameObject to lane items list
    /// </summary>
    /// <param name="gameObject">The gameObject to be added</param>
    public void AddItem(GameObject gameObject)
    {
        laneItems.Add(gameObject);
    }


    /// <summary>
    /// Destroyes lane and all its items
    /// </summary>
    public void DestroyLane()
    {
        if (laneItems != null)
        {
            for (int i = 0; i < laneItems.Count; i++)
                Destroy(laneItems[i]);
        }

        Destroy(gameObject);
    }
    #endregion
}
