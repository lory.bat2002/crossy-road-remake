using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadLane : Lane
{
    #region Variables & Properties
    Transform vehicleSpawn;
    GameObject spawnableVehicle;
    GameObject spawnedVehicle;

    float vehicleSpeed;
    int policeSpawn;

    [Space(20)]
    [Header("Lane Parameters")]
    [Tooltip("The minimum speed a spawned vehicle can go")]
    [SerializeField] float minVehicleSpeed;
    [Tooltip("The maximum speed a spawned vehicle can go")]
    [SerializeField] float maxVehicleSpeed;
    [Tooltip("The minimum time between vehicles spawns")]
    [SerializeField] float minSpawnTime;
    [Tooltip("The maximum time between vehicles spawns")]
    [SerializeField] float maxSpawnTime;
    [Tooltip("Which layerMask to look out for when placing stripes")]
    [SerializeField] LayerMask roadMask;
    [Tooltip("The police car spawn probability")]
    [SerializeField] int policeSpawnProbability;
    [Tooltip("How much extra time will the spawn wait for spawning the police car")]
    [SerializeField] float policeExtraSpawnWaitingTime;

    [Space(20)]
    [Header("Lane References")]
    [Tooltip("References to spawn objects used by the lane")]
    [SerializeField] Transform[] vehicleSpawns;
    [Tooltip("References to stripes spawn object used by the lane")]
    [SerializeField] Transform stripesSpawns;

    [Space(20)]
    [Header("Spawnable Lane Items")]
    [Tooltip("Reference to the stripes prefab")]
    [SerializeField] GameObject stripesItem;
    [Tooltip("Reference to the police car prefab")]
    [SerializeField] GameObject policeCar;
    [Tooltip("Which items can be spawned in this lane")]
    [SerializeField] GameObject[] laneSpawnableItems;
    #endregion


    #region Methods
    /// <summary>
    /// Initializes road lane keeping in mind whether a road has already been spawned in the adjacent lane
    /// </summary>
    /// <param name="nearRoad">Is a road already adjacent</param>
    public void InitializeLane(bool nearRoad)
    {
        int probability = Random.Range(0, 2);
        if (probability == 0)
            vehicleSpawn = vehicleSpawns[0];
        else
            vehicleSpawn = vehicleSpawns[1];

        spawnableVehicle = laneSpawnableItems[Random.Range(0, laneSpawnableItems.Length)];
        vehicleSpeed = Random.Range(minVehicleSpeed, maxVehicleSpeed);

        //If a road is ajacent, spawn stripes
        if (nearRoad)
            laneItems.Add(Instantiate(stripesItem, stripesSpawns.position, stripesSpawns.rotation));

        StartCoroutine(SpawnVehicle());
    }


    /// <summary>
    /// Regularly spawns vehicles
    /// </summary>
    /// <returns></returns>
    IEnumerator SpawnVehicle()
    {
        policeSpawn = Random.Range(1, 101);
        if (policeSpawn <= policeSpawnProbability)
        {
            yield return new WaitForSeconds(policeExtraSpawnWaitingTime);
            spawnedVehicle = Instantiate(policeCar, vehicleSpawn.position, vehicleSpawn.rotation);
            laneItems.Add(spawnedVehicle);
        }
        else
        {
            spawnedVehicle = Instantiate(spawnableVehicle, vehicleSpawn.position, vehicleSpawn.rotation);
            spawnedVehicle.GetComponent<Vehicle>().Speed = vehicleSpeed;
            laneItems.Add(spawnedVehicle);
        }
        yield return new WaitForSeconds(Random.Range(minSpawnTime, maxSpawnTime));

        StartCoroutine(SpawnVehicle());
    }


    /// <summary>
    /// Adds gameObject to lane items list
    /// </summary>
    /// <param name="gameObject">The gameObject to be added</param>
    public void AddItem(GameObject gameObject)
    {
        laneItems.Add(gameObject);
    }


    /// <summary>
    /// Destroyes lane and all its items
    /// </summary>
    public void DestroyLane()
    {
        if (laneItems != null)
        {
            for (int i = 0; i < laneItems.Count; i++)
                if (laneItems[i] != null)
                    Destroy(laneItems[i]);
        }

        Destroy(gameObject);
    }
    #endregion
}
