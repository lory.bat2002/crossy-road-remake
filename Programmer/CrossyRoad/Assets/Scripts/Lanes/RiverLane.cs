using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiverLane : Lane
{
    #region Variables & Properties
    Transform logSpawn;
    GameObject logRef;

    float logSpeed;

    [Space(20)]
    [Header("Lane Parameters")]
    [Tooltip("The minimum time between vehicles spawns")]
    [SerializeField] float minSpawnTime;
    [Tooltip("The maximum time between vehicles spawns")]
    [SerializeField] float maxSpawnTime;
    [Tooltip("The probability of the lane being a water lily lane instead of a log one")]
    [SerializeField] int waterLilyProbability;
    [Tooltip("The maximum amount of water lilies per each lane")]
    [SerializeField] int maxWaterLilyPerLane;
    [Tooltip("The minimum speed a log can spawn at")]
    [SerializeField] float minLogSpeed;
    [Tooltip("The maximum speed a log can spawn at")]
    [SerializeField] float maxLogSpeed;

    [Space(20)]
    [Header("Lane References")]
    [Tooltip("References to spawn objects used by the lane")]
    [SerializeField] Transform[] logSpawns;

    [Space(20)]
    [Header("Spawnable Lane Items")]
    [Tooltip("Reference to the spawnable water lily")]
    [SerializeField] GameObject waterLily;
    [Tooltip("Which items can be spawned in this lane")]
    [SerializeField] GameObject[] laneSpawnableItems;
    #endregion


    #region Mono
    //If player collides with the water, drown him
    private void OnCollisionEnter(Collision collision)
    {
        collision.gameObject.GetComponent<Player>().GameOver(GameOverType.DROWNED);
    }
    #endregion


    #region Methods
    /// <summary>
    /// Initializes river lane by placing obstances based on given probabilities (for lilypads)
    /// </summary>
    /// <param name="clearPaths">The bool array containing all obstacles forbidden placement positions</param>
    /// <param name="internalSpawnProbability">The probability of spawing obstacles inside the playable area</param>
    /// <param name="externalSpawnProbability">The probability of spawing obstacles outside the playable area</param>
    /// <param name="distribution">The obstacles distribution pattern</param>
    public void InitializeLane(bool[] clearPaths, int internalSpawnProbability, int externalSpawnProbability, ObstaclesDistribution distribution)
    {
        int probability = Random.Range(1, 101);

        if (probability <= waterLilyProbability)
        {
            for (int i = 0; i < clearPaths.Length && maxWaterLilyPerLane > 0; i++)
            {
                if (clearPaths[i])
                {
                    laneItems.Add(Instantiate(waterLily, transform.position + Vector3.right * (i - clearPaths.Length / 2), Quaternion.Euler(0f, Random.Range(0, 4) * 90f, 0f)));
                    maxWaterLilyPerLane--;
                } 
            }   
        }
        else
        {
            probability = Random.Range(0, 2);
            if (probability == 0)
                logSpawn = logSpawns[0];
            else
                logSpawn = logSpawns[1];

            logSpeed = Random.Range(minLogSpeed, maxLogSpeed);

            StartCoroutine(SpawnLog());
        }
    }


    /// <summary>
    /// Spawns floating logs
    /// </summary>
    /// <returns></returns>
    IEnumerator SpawnLog()
    {
        logRef = Instantiate(laneSpawnableItems[Random.Range(0, laneSpawnableItems.Length)], logSpawn.position, logSpawn.rotation);
        logRef.GetComponent<FloatingLog>().Speed = logSpeed;
        laneItems.Add(logRef);
        yield return new WaitForSeconds(Random.Range(minSpawnTime, maxSpawnTime));

        StartCoroutine(SpawnLog());
    }


    /// <summary>
    /// Destroyes lane and all its items
    /// </summary>
    public void DestroyLane()
    {
        if (laneItems != null)
        {
            for (int i = 0; i < laneItems.Count; i++)
                if (laneItems[i] != null)
                    Destroy(laneItems[i]);
        }

        Destroy(gameObject);
    }
    #endregion
}
